# README #

__SkiCenter Manager__    

SkiCenter Manager è un'applicazione creata con l'obiettivo di aiutare gli appassionati della montagna ad acquistare o noleggiare articoli per sci e snowboard e di permettere loro di accedere a servizi quali l'acquisto di skipass, l'iscrizione a lezioni tenute da un maestro qualificato e il deposito delle attrezzature.
Nel software, oltre alla modalità utente, sarà presente anche una modalità amministratore dalla quale è possibile visualizzare le operazioni fatte dagli utenti e il saldo del negozio.

### How do I get set up? ###

_Per accedere alla modalità amministratore devi usare una di queste credenziali:_

* 1) Username: tommi96, Password: 96tommi  [Amministratore= Tommaso Bombardi]   
* 2) Username: meri96,  Password: 96meri  [Amministratore= Maria Maddalena Mascellaro]  
* 3) Username: simo96,  Password: 96simo  [Amministratore= Simone Prati]

### Who do I talk to? ###

* Tommaso Bombardi: [tommaso.bombardi@studio.unibo.it](Link URL)   
* Maria Maddalena Mascellaro: [maria.mascellaro@studio.unibo.it](Link URL)   
* Simone Prati: [simone.prati2@studio.unibo.it](Link URL)