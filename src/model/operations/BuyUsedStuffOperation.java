package model.operations;

import model.admin.Pair;
/**
 * Class with the implementation of a buy used object operation.
 */
public class BuyUsedStuffOperation implements Operation {
    /**
     * 
     */
    private static final long serialVersionUID = 1L;
    private final String obj;
    private final String vendor;
    private final double price;
    private static final int GAIN = 5;

    /**
     * Constructor for a buy used object operation.
     * 
     * @param object is the name of the object.
     * @param owner is the username of the user who sell the object
     * @param cost is the price of the object.
     */
    public BuyUsedStuffOperation(final String object, final String owner, final double cost) {
        this.obj = object;
        this.vendor = owner;
        this.price = cost;
    }

    @Override
    public String getDescription() {
        return obj;
    }

    @Override
    public String getDetail() {
        return "Acquisto Usato";
    }

    @Override
    public String getInfo() {
        return "Oggetto= " +  this.obj + " // Venditore= " + this.vendor;
    }

    @Override
    public Double getPrice() {
        return price;
    }

    @Override
    public Pair<Double, Double> getGain() {
        return new Pair<>(this.getPrice(), (this.getPrice() / 100) * GAIN);
    }
}
