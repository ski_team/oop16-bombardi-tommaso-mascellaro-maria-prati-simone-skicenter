package model.operations;

import model.admin.products.Instructor;
import model.admin.products.BuyObject;
import model.admin.products.RentObject;
import model.admin.products.Season;
import model.admin.products.Skipass;

/**
 * Implementation for operation factory, designed using singleton pattern.
 */
public final class OperationFactoryImpl implements OperationFactory {

    private static final OperationFactory SINGLETON = new OperationFactoryImpl();

    /**
     * Private constructor for operation factory.
     */
    private OperationFactoryImpl() {
        super();
    }

    /**
     * Get operation factory.
     * 
     * @return the only one instance of operation factory
     */
    public static synchronized OperationFactory getOperationFactory() {
        return SINGLETON;
    }

    @Override
    public Operation createBuyOperation(final BuyObject obj, final int numObj) {
        return new BuyOperation(obj, numObj);
    }

    @Override
    public Operation createRentOperation(final RentObject obj, final int numObj, final int numDays, final Season season) {
        return new RentOperation(obj, numObj, numDays, season);
    }

    @Override
    public Operation createInstructorOperation(final Instructor inst, final int numSkiers, final Season season) {
        return new InstructorOperation(inst, numSkiers, season);
    }

    @Override
    public Operation createSkipassOperation(final Skipass skip, final int numObj, final Season season) {
        return new SkipassOperation(skip, numObj, season);
    }

    @Override
    public Operation createStorageOperation(final RentObject obj, final int numObj, final int numDays) {
        return new StorageOperation(obj, numObj, numDays);
    }

    @Override
    public Operation createBuyUsedStuffOperation(final String obj, final String vendor, final double price) {
        return new BuyUsedStuffOperation(obj, vendor, price);
    }

}
