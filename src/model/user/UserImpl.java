package model.user;

import java.text.NumberFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import model.admin.Demo;
import model.admin.Pair;
import model.operations.Operation;
import model.operations.OperationFactory;
import model.operations.OperationFactoryImpl;
import model.admin.products.Instructor;
import model.admin.products.BuyObject;
import model.admin.products.RentObject;
import model.admin.products.Season;
import model.admin.products.Skipass;
import model.user.interfaces.User;
/**
 * Class model User, using SINGLETON pattern.
 */
public final class UserImpl extends Demo implements User {

    /**
     * singleton pattern.
     */
    private static final User SINGLETON = new UserImpl();
    private static final OperationFactory OPFAC = OperationFactoryImpl.getOperationFactory();
    //CARRELLO
    private final Map<Integer, Operation> cart = new HashMap<>();
    private int countCart = 1; 
    //UTENTE
    private final Map<Pair<String, String>, Pair<String, String>> users;
    private Optional<Pair<String, String>> currentUser = Optional.empty();
    //ANNUNCI
    private final Map<Integer, Pair<String, Pair<String, Double>>> adsMap = new HashMap<>();
    private final Map<Integer, Pair<String, Pair<String, Double>>> selectedAds;
    private int countSelected = 1;

    //MAGIC NUMBERS AND REPS
    /**
     * months in a year.
     */
    public static final int MONTHS = 12;
    /**
     * String that repeats.
     */
    public static final String QUANTITY = "Quantita' : ";

    /**
     * initialize users map with demo users.
     */
    private UserImpl() {
        super();
        this.users = super.demoUsers();
        this.selectedAds = new HashMap<>();
    }
    /**
     * Get an instance of model user class.
     * @return the only one instance of user model
     */
    public static synchronized User getModelUser() {
        return SINGLETON;
    }

    //LOGIN UTENTE
    @Override
    public void register(final Pair<String, String> userPass, final Pair<String, String> nameSurname) {
        for (final Pair<String, String> p : users.keySet()) {
            if (userPass.getX().equals(p.getX())) {
                //USERNAME ALREADY EXIST
                throw new IllegalStateException();
            }
        }
        //CREATE NEW USER
        users.put(userPass, nameSurname);
        this.checkLogin(userPass.getX(), userPass.getY());
    }
    @Override
    public void checkLogin(final String username, final String password) {
        boolean login = false;
        for (final Pair<String, String> p : users.keySet()) {
            if (username.equals(p.getX()) && password.equals(p.getY())) {
                //LOGIN OK
                this.currentUser = Optional.of(new Pair<>(username, password));
                login = true;
            }
        }
        if (!login) {
            throw new UnsupportedOperationException();
        }
    }
    @Override
    public Optional<String> getCurrentUsername() {
        return Optional.of(this.currentUser.get().getX());
    }
    @Override
    public Optional<String> getLoggedUserName() {
        if (this.currentUser.isPresent()) {
            return Optional.of(this.users.get(this.currentUser.get()).getX() + " " + this.users.get(this.currentUser.get()).getY());
        }
        return Optional.empty();
    }
    @Override
    public void logout() {
        this.currentUser = Optional.empty();
    }

    //MEMORIZZAZIONE UTENTI
    @Override
    public void addUsers(final Map<Pair<String, String>, Pair<String, String>> usersMap) {
        for (final Pair<String, String> p : usersMap.keySet()) {
            if (!this.getUsers().containsKey(p)) {
                this.users.put(p, usersMap.get(p));
            }
        }
    }
    @Override
    public Map<Pair<String, String>, Pair<String, String>> getUsers() {
        return users;
    }
    @Override
    public void resetUsers() {
        this.users.clear();
        this.users.putAll(super.demoUsers());
    }

    //UTENTE
    @Override
    public Map<Integer, Pair<String, Operation>> checkout(final int savedOps) {
        final Map<Integer, Pair<String, Operation>> completedOps = new HashMap<>();
        for (final int i : this.cart.keySet()) {
            completedOps.put(i + savedOps, new Pair<>(this.currentUser.get().getX(), this.cart.get(i)));
        }
        this.selectedAds.clear();
        this.currentUser = Optional.empty();
        this.emptyCart();
        return completedOps;
    }

    //CARRELLO
    @Override
    public void buyProduct(final int quantity, final BuyObject obj) {
        cart.put(this.countCart, OPFAC.createBuyOperation(obj, quantity));
        this.countCart++;
    }
    @Override
    public void rentProduct(final int quantity, final RentObject obj, final int duration, final Season season) {
        cart.put(this.countCart, OPFAC.createRentOperation(obj, quantity, duration, season));
        this.countCart++;
    }
    @Override
    public void depositProduct(final int numObj, final int duration, final RentObject obj) {
        cart.put(this.countCart, OPFAC.createStorageOperation(obj, numObj, duration));
        this.countCart++;
    }
    @Override
    public void bookLesson(final int numStudents, final Season season, final Instructor instructor) {
        cart.put(this.countCart, OPFAC.createInstructorOperation(instructor, numStudents, season));
        this.countCart++;
    }
    @Override
    public void buySkiPass(final int quantity, final Skipass skipass, final Season season) {
        cart.put(this.countCart, OPFAC.createSkipassOperation(skipass, quantity, season));
        this.countCart++;
    }
    @Override
    public void buyUsedStuff(final int index) {
        cart.put(this.countCart, OPFAC.createBuyUsedStuffOperation(this.adsMap.get(index).getY().getX(), 
                this.adsMap.get(index).getX(), this.adsMap.get(index).getY().getY()));
        this.countCart++;
        this.selectedAds.put(this.countSelected, this.adsMap.get(index));
        this.countSelected++;
    }
    @Override
    public void removeFromCart(final int index) {
        if (index <= cart.size()) {
            if (this.cart.get(index).getDetail().equals("Acquisto Usato")) {
                for (final int elem : this.selectedAds.keySet()) {
                    if (this.selectedAds.get(elem).equals(new Pair<>(this.cart.get(index).getInfo().split(" ")[this.cart.get(index).getInfo().split(" ").length - 1], 
                            new Pair<>(this.cart.get(index).getDescription(), this.cart.get(index).getPrice())))) {
                        this.addAd(this.cart.get(index).getInfo().split(" ")[this.cart.get(index).getInfo().split(" ").length - 1], 
                                this.cart.get(index).getDescription(), this.cart.get(index).getPrice());
                        this.selectedAds.remove(elem);
                        break;
                    }
                }
            }
            for (int i = index; i < cart.keySet().size(); i++) {
                cart.replace(i, cart.get(i), cart.get(i + 1));
            }
            cart.remove(cart.keySet().size());
            this.countCart--;
        } else {
            throw new IllegalArgumentException();
        }
    }
    @Override
    public Map<Integer, Operation> getCart() {
        return this.cart;
    }
    @Override
    public void emptyCart() {
        if (!this.selectedAds.isEmpty()) {
            for (final int i : this.selectedAds.keySet()) {
                this.addAd(this.selectedAds.get(i).getX(), this.selectedAds.get(i).getY().getX(), this.selectedAds.get(i).getY().getY());
            }
        }
        this.selectedAds.clear();
        this.countCart = 1;
        this.cart.clear();
    }
    @Override
    public String getCartTotalPrice() {
        double totalPrice = 0;
        for (final int i : this.cart.keySet()) {
            totalPrice = totalPrice + this.cart.get(i).getPrice();
        }
        return this.round(totalPrice);
    }

    //PAGAMENTO
    @Override
    public void checkPayment(final String code, final String date)  {
        if (!this.checkDate(date) || !this.checkCardCode(code)) {
            throw new IllegalArgumentException();
        }
    }
    //CHECK PER IL PAGAMENTO
    private boolean checkCardCode(final String code) {
        if (!code.isEmpty() && code.length() == 16) {
            try {
                Long.parseLong(code);
            } catch (NumberFormatException e) { 
                return false;
            }
            return true;
        }
        return false;
    }
    private boolean checkDate(final String date) {
        final String expireMonth = date.substring(0, 2);
        final String expireYear = date.substring(3, 7);
        int month = 0;
        int year = 0;
        final Calendar today = Calendar.getInstance();

        if (date.substring(2, 3).equals("/")) {
            try {
                month = Integer.parseInt(expireMonth);
                year = Integer.parseInt(expireYear);
            } catch (NumberFormatException e) { 
                return false;
            }
            if ((month <= MONTHS) && (month > 0)) {
                if (year > today.get(Calendar.YEAR)) {
                    return true;
                } else if (year == today.get(Calendar.YEAR) && month >= today.get(Calendar.MONTH)) {
                    return true;
                }
            }
        }
        return false;
    }

    //ARROTONDAMENTO PREZZI
    private String round(final double num) {
        final NumberFormat numForm = NumberFormat.getInstance();
        numForm.setMaximumFractionDigits(2);
        numForm.setMinimumFractionDigits(2);
        return numForm.format(num);
    }

    //GESTIONE ANNUNCI
    @Override
    public void addAd(final String vendor, final String obj, final double price) {
        this.adsMap.put(this.adsMap.size() + 1, new Pair<>(vendor, new Pair<>(obj, price)));
    }
    @Override
    public void removeAd(final int index) {
        if (index <= adsMap.size()) {
            for (int i = index; i < adsMap.keySet().size(); i++) {
                adsMap.replace(i, adsMap.get(i), adsMap.get(i + 1));
            }
            adsMap.remove(adsMap.keySet().size());
        } else {
            throw new IllegalArgumentException();
        }
    }
    @Override
    public void importAds(final Map<Integer, Pair<String, Pair<String, Double>>> ads) {
        for (final int i : ads.keySet()) {
            if (!this.adsMap.containsKey(i)) {
                this.adsMap.put(i, ads.get(i));
            }
        }
    }
    @Override
    public Map<Integer, Pair<String, Pair<String, Double>>> getAdsMap() {
        return this.adsMap;
    }
    @Override
    public Map<Integer, Pair<String, Pair<String, Double>>> getSelectedAds() {
        return this.selectedAds;
    }
}