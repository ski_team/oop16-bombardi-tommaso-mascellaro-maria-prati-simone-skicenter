package model.user.interfaces;

import java.util.Map;

import model.admin.Pair;
/**
 *
 */
public interface UsedStuffAds {
    /**
     * @param vendor username of who sel the object 
     * @param obj name of th object
     * @param price cost of the object
     */
    void addAd(String vendor, String obj, double price);
    /**
     * @param index of the ad has to be sold
     */
    void removeAd(int index);
    /**
     * @param ads map of ads saved in a file
     */
    void importAds(Map<Integer, Pair<String, Pair<String, Double>>> ads);
    /**
     * @return the map that contains the ads
     */
    Map<Integer, Pair<String, Pair<String, Double>>> getAdsMap();
    /**
     * @return the map that contains the ads added to cart
     */
    Map<Integer, Pair<String, Pair<String, Double>>> getSelectedAds();
}
