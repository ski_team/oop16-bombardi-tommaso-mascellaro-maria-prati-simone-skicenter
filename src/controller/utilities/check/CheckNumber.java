package controller.utilities.check;

import java.util.Optional;

/**
 * Interface for check number algorithm.
 */
public interface CheckNumber {

    /**
     * Check number.
     * 
     * @param num
     *              string which represents the number
     * @param max
     *              integer used to implement the check algorithm,
     *              in the implementation done in the factory it represents the max value the number can have
     *              and this integer is -1 if there isn't a max value
     * @return an optional
     *              this optional can have different meanings according to the algorithm,
     *              in the implementation done in the factory it is empty if the string is not valid 
     *              or if the number represented by the string is lower than one,
     *              otherwise it's with an integer which represents the number
     */
    Optional<Integer> check(String num, int max);

}
