package view;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

import controller.Controller;
import model.admin.Pair;

import view.panels.FrameSize;
import view.panels.PanelBaseImpl;
import view.panels.admin.PanelAdminOneImpl;
import view.panels.admin.PanelLoginAdminImpl;
import view.panels.admin.PanelTableImpl;
import view.panels.interfaces.PanelAdminOne;
import view.panels.interfaces.PanelBase;
import view.panels.interfaces.PanelBuy;
import view.panels.interfaces.PanelBuyDetails;
import view.panels.interfaces.PanelCart;
import view.panels.interfaces.PanelLoginAdmin;
import view.panels.interfaces.PanelLoginProfile;
import view.panels.interfaces.PanelProfile;
import view.panels.interfaces.PanelPurchase;
import view.panels.interfaces.PanelRentStorage;
import view.panels.interfaces.PanelRentStorageDetails;
import view.panels.interfaces.PanelSkipassInstructor;
import view.panels.interfaces.PanelTable;
import view.panels.interfaces.PanelUsedAds;
import view.panels.interfaces.PanelUserOne;
import view.panels.store.PanelBuyDetailsImpl;
import view.panels.store.PanelBuyImpl;
import view.panels.store.PanelInstructorImpl;
import view.panels.store.PanelRentDetailsImpl;
import view.panels.store.PanelRentImpl;
import view.panels.store.PanelSkipassImpl;
import view.panels.store.PanelStorageDetailsImpl;
import view.panels.store.PanelStorageImpl;
import view.panels.user.PanelCartImpl;
import view.panels.user.PanelLoginProfileImpl;
import view.panels.user.PanelProfileImpl;
import view.panels.user.PanelPurchaseImpl;
import view.panels.user.PanelUsedAdsImpl;
import view.panels.user.PanelUserOneImpl;

/**
 * Main Class for SkiCenter GUI.
 *
 */
public class SkiCenterGUI extends JFrame {
    private static final long serialVersionUID = 1L;
    private final Controller ci;
    private final PanelBase panelBasicObj;
    private final PanelLoginAdmin panelLoginAdminObj;
    private final PanelUserOne panelUserOneObj;
    private final PanelSkipassInstructor panelSkipassObj;
    private final PanelAdminOne panelAdminOneObj;
    private final PanelBuy panelBuyObj;
    private final PanelRentStorage panelRentObj;
    private final PanelSkipassInstructor panelInstructorObj;
    private final PanelRentStorage panelStorageObj;
    private final PanelTable panelTableObj;
    private final PanelCart panelCartObj;
    private final PanelPurchase panelPurchaseObj;
    private final PanelBuyDetails panelBuyDetailsObj;
    private final PanelRentStorageDetails panelRentDetailsObj;
    private final PanelRentStorageDetails panelStorageDetailsObj;

    //PRATI
    private final PanelProfile panelProfileObj;
    private final PanelUsedAds panelUsedAdsObj;
    private final PanelLoginProfile panelLoginProfileObj;

    //CAMBIA PANNELLI rp PANNELLO DA RIMUOVERE, ap DA AGGIUNGERE
    private void switchPanel(final JPanel rp, final JPanel ap) {
        panelBasicObj.getPanelBase().remove(rp);
        panelBasicObj.getPanelBase().add(ap);
        ap.setVisible(true);
        rp.setVisible(false);
    }
    //STAMPA CARRELLO
    private void printCart() {
        for (final Integer i : ci.getCart().keySet()) {
            panelCartObj.addTableRow(i, new Pair<String, Pair<String, String>>(
                    ci.getCartOperationGain(i).getX(), new Pair<>(ci.getCart().get(i).getDetail(),
                            ci.getCart().get(i).getInfo())));
            panelCartObj.addLabelPrice(ci.getCartPrice());
            panelCartObj.getBtnDeleteCart().setEnabled(true);
            panelCartObj.getBtnDeleteOperation().setEnabled(true);
            panelCartObj.getBtnFinishOp().setEnabled(true);
            panelCartObj.getOp().setEnabled(true);
        }
        if (ci.getCart().keySet().isEmpty()) {
            panelCartObj.addLabelPrice("0,00");
            panelCartObj.getBtnDeleteCart().setEnabled(false);
            panelCartObj.getBtnDeleteOperation().setEnabled(false);
            panelCartObj.getBtnFinishOp().setEnabled(false);
            panelCartObj.getOp().setEnabled(false);
        }
    }
    private void printAds() {
        for (final Integer i : ci.getAdsMap().keySet()) {
            panelUsedAdsObj.addTableRow(i, ci.getAdsMap().get(i));
        }
    }
    //DISABILITA
    private void disableComp() {
        panelPurchaseObj.getBtnPay().setEnabled(true);
        panelPurchaseObj.getTextUser().setEnabled(false);
        panelPurchaseObj.getTextUser2().setEnabled(false);
        panelPurchaseObj.getTextPass().setEnabled(false);
        panelPurchaseObj.getTextPass2().setEnabled(false);
        panelPurchaseObj.getTextName().setEnabled(false);
        panelPurchaseObj.getTextSurname().setEnabled(false);
        panelPurchaseObj.getBtnLogin().setEnabled(false);
        panelPurchaseObj.getBtnReg().setEnabled(false);
        panelPurchaseObj.getTextCard().setEnabled(true);
        panelPurchaseObj.getTextOwner().setEnabled(true);
        panelPurchaseObj.getTextDate().setEnabled(true);
        panelPurchaseObj.getTextCvc().setEnabled(true);
    }
    //ABILITA
    private void enableComp() {
        panelPurchaseObj.getBtnPay().setEnabled(false);
        panelPurchaseObj.getTextUser().setEnabled(true);
        panelPurchaseObj.getTextUser2().setEnabled(true);
        panelPurchaseObj.getTextPass().setEnabled(true);
        panelPurchaseObj.getTextPass2().setEnabled(true);
        panelPurchaseObj.getTextName().setEnabled(true);
        panelPurchaseObj.getTextSurname().setEnabled(true);
        panelPurchaseObj.getBtnLogin().setEnabled(true);
        panelPurchaseObj.getBtnReg().setEnabled(true);
        panelPurchaseObj.getTextCard().setEnabled(false);
        panelPurchaseObj.getTextOwner().setEnabled(false);
        panelPurchaseObj.getTextDate().setEnabled(false);
        panelPurchaseObj.getTextCvc().setEnabled(false);
    }
    //SET JTEXTFIELD EMPTY
    private void setTextEmpty(final JTextField text) {
        text.setText("");
    }

    /**
     * Constructor for SkiCenterGUI.
     * @throws IOException
     *              Exc IOException
     */
    public SkiCenterGUI() throws IOException {
        super();
        this.ci = Controller.getController();
        this.panelBasicObj = new PanelBaseImpl();
        this.panelLoginAdminObj = new PanelLoginAdminImpl();
        this.panelUserOneObj = new PanelUserOneImpl();
        this.panelSkipassObj = new PanelSkipassImpl();
        this.panelAdminOneObj = new PanelAdminOneImpl();
        this.panelBuyObj = new PanelBuyImpl();
        this.panelRentObj = new PanelRentImpl();
        this.panelInstructorObj = new PanelInstructorImpl();
        this.panelStorageObj = new PanelStorageImpl();
        this.panelTableObj = new PanelTableImpl();
        this.panelCartObj = new PanelCartImpl();
        this.panelPurchaseObj = new PanelPurchaseImpl();
        this.panelBuyDetailsObj = new PanelBuyDetailsImpl();
        this.panelRentDetailsObj = new PanelRentDetailsImpl();
        this.panelStorageDetailsObj = new PanelStorageDetailsImpl();

        //PRATI
        this.panelUsedAdsObj = new PanelUsedAdsImpl();
        this.panelLoginProfileObj = new PanelLoginProfileImpl();
        this.panelProfileObj = new PanelProfileImpl();

        panelBasicObj.getPanelBase().add(panelBasicObj.getPanelWelcome());
        panelBasicObj.getPanelWelcome().setVisible(true);

        /////////////PARTE PROFILO//////////////////
        //LOGIN PROFILO
        panelBasicObj.getBtnProfilePanel().addActionListener(e -> {
            switchPanel(panelBasicObj.getPanelWelcome(), panelLoginProfileObj.getPanel());
        });
        //DA LOGIN PROFILO A PANEL WELCOME
        panelLoginProfileObj.getBtnPrev().addActionListener(e -> {
            switchPanel(panelLoginProfileObj.getPanel(), panelBasicObj.getPanelWelcome());
        });
        //LOGIN UTENTE REGISTRATO
        panelLoginProfileObj.getBtnLogin().addActionListener(e-> {
                boolean login = true;
                try {
                    ci.loginUser(panelLoginProfileObj.getTextUser().getText(), panelLoginProfileObj.getTextPass().getText());
                } catch (IllegalArgumentException exc) {
                    JOptionPane.showMessageDialog(null, "Username e password devono avere da 5 a 8 caratteri");
                    login = false;
                } catch (UnsupportedOperationException exc1) {
                    JOptionPane.showMessageDialog(null, "Le credenziali non corrispondono a nessun utente");
                    login = false;
                }
                if (login) {
                    JOptionPane.showMessageDialog(null, "Login effettuato correttamente");
                    //panelLoginProfileObj.addLabel(ci.getCurrentUser().get());
                    disableComp();
                    panelProfileObj.addLabel(ci.getCurrentUser().get());
                    for (final Integer elem : ci.getUserOperations(ci.getLoggedUsername().get())) {
                        panelProfileObj.addTableRow(new Pair<>(elem, ci.getOperations().get(elem).getX()),
                                ci.getOperations().get(elem).getY().getDetail(),
                                ci.getOperations().get(elem).getY().getInfo(), ci.getOperationGain(elem));
                    }
                    switchPanel(panelLoginProfileObj.getPanel(), panelProfileObj.getPanel());
                } else {
                    setTextEmpty(panelLoginProfileObj.getTextUser());
                    setTextEmpty(panelLoginProfileObj.getTextPass());
                }
        });
        //REGISTRAZIONE NUOVO UTENTE
        panelLoginProfileObj.getBtnReg().addActionListener(e-> {
                boolean login = true;
                try {
                    ci.registerUser(new Pair<String, String>(panelLoginProfileObj.getTextUser2().getText(), panelLoginProfileObj.getTextPass2().getText()), 
                            new Pair<String, String>(panelLoginProfileObj.getTextName().getText(), panelLoginProfileObj.getTextSurname().getText()));
                } catch (IllegalArgumentException exc) {
                    JOptionPane.showMessageDialog(null, "Le credenziali devono essere stringhe da 5 a 8 caratteri");
                    login = false;
                } catch (IllegalStateException exc1) {
                    JOptionPane.showMessageDialog(null, "le credenziali inserite corrispondono a un altro utente già registrato");
                    login = false;
                } catch (UnsupportedOperationException exc2) {
                    System.err.println("Utente non memorizzato correttamente su file");
                }
                if (login) {
                    JOptionPane.showMessageDialog(null, "Registrazione effettuata correttamente");
                    disableComp();
                    panelProfileObj.addLabel(ci.getCurrentUser().get());
                    switchPanel(panelLoginProfileObj.getPanel(), panelProfileObj.getPanel());
                }
        });
        //LOGOUT PROFILE
        panelProfileObj.getBtnPrev().addActionListener(e-> {
                switchPanel(panelProfileObj.getPanel(), panelLoginProfileObj.getPanel());
                enableComp();
                setTextEmpty(panelLoginProfileObj.getTextUser());
                setTextEmpty(panelLoginProfileObj.getTextPass());
                setTextEmpty(panelLoginProfileObj.getTextUser2());
                setTextEmpty(panelLoginProfileObj.getTextPass2());
                setTextEmpty(panelLoginProfileObj.getTextName());
                setTextEmpty(panelLoginProfileObj.getTextSurname());
                panelProfileObj.deleteLabel();
                panelProfileObj.deleteTable();
                ci.logoutUser();
        });
        //TASTO AGGIUNGI ANNUNCIO
        panelProfileObj.getBtnAddAd().addActionListener(e -> {
            String input;
            String input2;
            input = JOptionPane.showInputDialog("Nome oggetto da vendere:");
            if (input != null) {
                if (input.isEmpty()) {
                    JOptionPane.showMessageDialog(null, "Inserisci il nome dell'oggetto.");
                }
                if (!input.isEmpty()) {
                    input2 = JOptionPane.showInputDialog("Prezzo:");
                    try {
                        if (input2 != null) {
                            ci.addAdvertisment(ci.getLoggedUsername().get(), input, Double.parseDouble(input2));
                        }
                    } catch (NumberFormatException exc) {
                        JOptionPane.showMessageDialog(null, "Inserisci dei numeri.");
                    }
                } 
            }
        });

        ///////////////PARTE UTENTE/////////////////
        //MENU USER
        panelBasicObj.getBtnUserPanel().addActionListener(e-> {
                switchPanel(panelBasicObj.getPanelWelcome(), panelUserOneObj.getPanel());
        });
        //DA MENU USER A PANEL WELCOME
        panelUserOneObj.getBtnPrev().addActionListener(e-> {
                switchPanel(panelUserOneObj.getPanel(), panelBasicObj.getPanelWelcome());
        });
        //PAGINA DI SKIPASS
        panelUserOneObj.getBtnSkipass().addActionListener(e-> {
                switchPanel(panelUserOneObj.getPanel(), panelSkipassObj.getPanel());
        });
        //DA SKIPASS A MENU USER
        panelSkipassObj.getBtnPrev().addActionListener(e-> {
                switchPanel(panelSkipassObj.getPanel(), panelUserOneObj.getPanel());
        });
        //PAGINA DI ACQUISTI
        panelUserOneObj.getBtnBuy().addActionListener(e-> {
                switchPanel(panelUserOneObj.getPanel(), panelBuyObj.getPanel());
        });
        //DA ACQUISTI A MENU USER
        panelBuyObj.getBtnPrev().addActionListener(e-> {
                switchPanel(panelBuyObj.getPanel(), panelUserOneObj.getPanel());
        });
        //DETTAGLIO ACQUISTI
        for (final JButton jb : panelBuyObj.getBtnObj().keySet()) {
            jb.addActionListener(e-> {
                    panelBuyDetailsObj.setObject(panelBuyObj.getBtnObj().get(jb));
                    switchPanel(panelBuyObj.getPanel(), panelBuyDetailsObj.getPanel());
                    panelBuyDetailsObj.addDetails();
            });
        }
        //DA DETTAGLIO ACQUISTI A MENU
        panelBuyDetailsObj.getBtnPrev().addActionListener(e-> {
                panelBuyDetailsObj.resetObject();
                panelBuyDetailsObj.removeDetails();
                switchPanel(panelBuyDetailsObj.getPanel(), panelBuyObj.getPanel());
        });
        //PAGINA DI NOLEGGIO
        panelUserOneObj.getBtnRent().addActionListener(e-> {
                switchPanel(panelUserOneObj.getPanel(), panelRentObj.getPanel());
        });
        //DA NOLEGGIO A MENU USER
        panelRentObj.getBtnPrev().addActionListener(e-> {
                switchPanel(panelRentObj.getPanel(), panelUserOneObj.getPanel());
        });
        //DETTAGLIO NOLEGGIO
        for (final JButton jb : panelRentObj.getBtnObj().keySet()) {
            jb.addActionListener(e-> {
                    panelRentDetailsObj.setObject(panelRentObj.getBtnObj().get(jb));
                    switchPanel(panelRentObj.getPanel(), panelRentDetailsObj.getPanel());
                    panelRentDetailsObj.addDetails();
            });
        }
        //TORNA ALLA PAGINA DI NOLEGGIO
        panelRentDetailsObj.getBtnPrev().addActionListener(e-> {
                panelRentDetailsObj.resetObject();
                panelRentDetailsObj.removeDetails();
                switchPanel(panelRentDetailsObj.getPanel(), panelRentObj.getPanel());
        });
        //PAGINA MAESTRO
        panelUserOneObj.getBtnInstructor().addActionListener(e-> {
                switchPanel(panelUserOneObj.getPanel(), panelInstructorObj.getPanel());
        });
        //DA PAGINA MAESTRO A MENU
        panelInstructorObj.getBtnPrev().addActionListener(e-> {
                switchPanel(panelInstructorObj.getPanel(), panelUserOneObj.getPanel());
        });
        //PAGINA DI DEPOSITO
        panelUserOneObj.getBtnStorage().addActionListener(e-> {
                switchPanel(panelUserOneObj.getPanel(), panelStorageObj.getPanel());
        });
        //DA DEPOSITO AL MENU USER
        panelStorageObj.getBtnPrev().addActionListener(e-> {
                switchPanel(panelStorageObj.getPanel(), panelUserOneObj.getPanel());
        });
        //DETTAGLIO DEPOSITO
        for (final JButton jb : panelStorageObj.getBtnObj().keySet()) {
            jb.addActionListener(e-> {
                    panelStorageDetailsObj.setObject(panelStorageObj.getBtnObj().get(jb));
                    switchPanel(panelStorageObj.getPanel(), panelStorageDetailsObj.getPanel());
                    panelStorageDetailsObj.addDetails();
            });
        }
        //DA DETTAGLIO DEPOSITO A MENU DEPOSITO
        panelStorageDetailsObj.getBtnPrev().addActionListener(e-> {
                panelStorageDetailsObj.resetObject();
                panelStorageDetailsObj.removeDetails();
                switchPanel(panelStorageDetailsObj.getPanel(), panelStorageObj.getPanel());
        });
        //PRATI
        //PAGINA ANNUNCI USATO
        panelUserOneObj.getBtnUsedAds().addActionListener(e -> {
            panelUsedAdsObj.deleteTable();
            printAds();
            switchPanel(panelUserOneObj.getPanel(), panelUsedAdsObj.getPanel());
        });
        //DA PAGINA ANNINCI USATO A MENU USER
        panelUsedAdsObj.getBtnPrev().addActionListener(e -> {
            panelUsedAdsObj.deleteTable();
            panelUsedAdsObj.getAd().setText("");
            switchPanel(panelUsedAdsObj.getPanel(), panelUserOneObj.getPanel());
        });
        //ACQUISTA UN OGGETTO DAGLI ANNUNCI
        panelUsedAdsObj.getBtnAddCart().addActionListener(e-> {
            try {
                ci.addUsedObj(panelUsedAdsObj.getObjectBuy());
                ci.removeAd(String.valueOf(panelUsedAdsObj.getObjectBuy()));
                JOptionPane.showMessageDialog(null, "Prodotto aggiunto al carrello");
            } catch (IllegalArgumentException exc) {
                JOptionPane.showMessageDialog(null, "L'oggetto inserito non è presente in bacheca");
            }
            panelUsedAdsObj.deleteTable();
            printAds();
            panelUsedAdsObj.getAd().setText("");
        });
        //VAI AL CARRELLO
        panelUserOneObj.getBtnCart().addActionListener(e-> {
                printCart();
                switchPanel(panelUserOneObj.getPanel(), panelCartObj.getPanel());
        });
        panelSkipassObj.getBtnCart().addActionListener(e-> {
                printCart();
                switchPanel(panelSkipassObj.getPanel(), panelCartObj.getPanel());
        });
        panelRentObj.getBtnCart().addActionListener(e-> {
                printCart();
                switchPanel(panelRentObj.getPanel(), panelCartObj.getPanel());
        });
        panelRentDetailsObj.getBtnCart().addActionListener(e-> {
                printCart();
                panelRentDetailsObj.removeDetails();
                switchPanel(panelRentDetailsObj.getPanel(), panelCartObj.getPanel());
        });
        panelBuyObj.getBtnCart().addActionListener(e-> {
                printCart();
                switchPanel(panelBuyObj.getPanel(), panelCartObj.getPanel());
        });
        panelBuyDetailsObj.getBtnCart().addActionListener(e-> {
                printCart();
                panelBuyDetailsObj.removeDetails();
                switchPanel(panelBuyDetailsObj.getPanel(), panelCartObj.getPanel());
        });
        panelInstructorObj.getBtnCart().addActionListener(e-> {
                printCart();
                switchPanel(panelInstructorObj.getPanel(), panelCartObj.getPanel());
        });
        panelStorageObj.getBtnCart().addActionListener(e-> {
                printCart();
                switchPanel(panelStorageObj.getPanel(), panelCartObj.getPanel());
        });
        panelStorageDetailsObj.getBtnCart().addActionListener(e-> {
                printCart();
                panelStorageDetailsObj.removeDetails();
                switchPanel(panelStorageDetailsObj.getPanel(), panelCartObj.getPanel());
        });
        panelCartObj.getBtnPrev().addActionListener(e-> {
                panelCartObj.deleteTable();
                switchPanel(panelCartObj.getPanel(), panelUserOneObj.getPanel());
        });
        panelUsedAdsObj.getBtnCart().addActionListener(e -> {
            panelUsedAdsObj.deleteTable();
            printCart();
            switchPanel(panelUsedAdsObj.getPanel(), panelCartObj.getPanel());
        });
        //ELIMINA TUTTO IL CARRELLO
        panelCartObj.getBtnDeleteCart().addActionListener(e-> {
                ci.removeAllOperations();
                panelCartObj.deleteTable();
                printCart();
                panelCartObj.addLabelPrice("0,00");
        });
        //ELIMINA UN'OPERAZIONE
        panelCartObj.getBtnDeleteOperation().addActionListener(e-> {
                try {
                    ci.removeOperation(panelCartObj.getOperationDelete());
                } catch (IllegalArgumentException exc) {
                    JOptionPane.showMessageDialog(null, "L'operazione inserita non è presente nel carrello");
                }
                panelCartObj.deleteTable();
                printCart();
                panelCartObj.getOp().setText("");
        });
        //VAI ALLA PAGINA DI ACQUISTO CON LOGIN
        panelCartObj.getBtnFinishOp().addActionListener(e-> {
                enableComp();
                switchPanel(panelCartObj.getPanel(), panelPurchaseObj.getPanel());
        });
        //DA PAGINA DI ACQUISTO A CARRELLO
        panelPurchaseObj.getBtnPrev().addActionListener(e-> {
                switchPanel(panelPurchaseObj.getPanel(), panelCartObj.getPanel());
                enableComp();
                setTextEmpty(panelPurchaseObj.getTextUser());
                setTextEmpty(panelPurchaseObj.getTextPass());
                setTextEmpty(panelPurchaseObj.getTextUser2());
                setTextEmpty(panelPurchaseObj.getTextPass2());
                setTextEmpty(panelPurchaseObj.getTextName());
                setTextEmpty(panelPurchaseObj.getTextSurname());
                setTextEmpty(panelPurchaseObj.getTextCard());
                setTextEmpty(panelPurchaseObj.getTextOwner());
                setTextEmpty(panelPurchaseObj.getTextDate());
                setTextEmpty(panelPurchaseObj.getTextCvc());
                panelPurchaseObj.deleteLabel();
        });
        //LOGIN UTENTE REGISTRATO
        panelPurchaseObj.getBtnLogin().addActionListener(e-> {
                boolean login = true;
                try {
                    ci.loginUser(panelPurchaseObj.getTextUser().getText(), panelPurchaseObj.getTextPass().getText());
                } catch (IllegalArgumentException exc) {
                    JOptionPane.showMessageDialog(null, "Username e password devono avere da 5 a 8 caratteri");
                    login = false;
                } catch (UnsupportedOperationException exc1) {
                    JOptionPane.showMessageDialog(null, "Le credenziali non corrispondono a nessun utente");
                    login = false;
                }
                if (login) {
                    JOptionPane.showMessageDialog(null, "Login effettuato correttamente");
                    panelPurchaseObj.addLabel(ci.getCurrentUser().get());
                    disableComp();
                } else {
                    setTextEmpty(panelPurchaseObj.getTextUser());
                    setTextEmpty(panelPurchaseObj.getTextPass());
                }
        });
        //REGISTRAZIONE NUOVO UTENTE
        panelPurchaseObj.getBtnReg().addActionListener(e-> {
                boolean login = true;
                try {
                    ci.registerUser(new Pair<String, String>(panelPurchaseObj.getTextUser2().getText(), panelPurchaseObj.getTextPass2().getText()), 
                            new Pair<String, String>(panelPurchaseObj.getTextName().getText(), panelPurchaseObj.getTextSurname().getText()));
                } catch (IllegalArgumentException exc) {
                    JOptionPane.showMessageDialog(null, "Le credenziali devono essere stringhe da 5 a 8 caratteri");
                    login = false;
                } catch (IllegalStateException exc1) {
                    JOptionPane.showMessageDialog(null, "le credenziali inserite corrispondono a un altro utente già registrato");
                    login = false;
                } catch (UnsupportedOperationException exc2) {
                    System.err.println("Utente non memorizzato correttamente su file");
                }
                if (login) {
                    JOptionPane.showMessageDialog(null, "Registrazione effettuata correttamente");
                    disableComp();
                    panelPurchaseObj.addLabel(ci.getCurrentUser().get());
                }
        });
        //PAGAMENTO
        panelPurchaseObj.getBtnPay().addActionListener(e-> {
                boolean pay = true;
                try {
                    ci.pay(new Pair<String, String>(panelPurchaseObj.getTextOwner().getText(), panelPurchaseObj.getTextCard().getText()),
                            new Pair<String, String>(panelPurchaseObj.getTextDate().getText(), panelPurchaseObj.getTextCvc().getText()));
                } catch (IllegalArgumentException exc) {
                    pay = false;
                    JOptionPane.showMessageDialog(null, "Non è stata inserita una carta di credito valida");
                }
                if (pay) {
                    try {
                        ci.completeOperations();
                    } catch (IllegalArgumentException exc) {
                        System.err.println("Operazioni non memorizzate correttamente su file");
                    }
                    JOptionPane.showMessageDialog(null, "Pagamento effettuato con successo");
                    switchPanel(panelPurchaseObj.getPanel(), panelUserOneObj.getPanel());
                    panelCartObj.deleteTable();
                    setTextEmpty(panelPurchaseObj.getTextUser());
                    setTextEmpty(panelPurchaseObj.getTextPass());
                    setTextEmpty(panelPurchaseObj.getTextUser2());
                    setTextEmpty(panelPurchaseObj.getTextPass2());
                    setTextEmpty(panelPurchaseObj.getTextName());
                    setTextEmpty(panelPurchaseObj.getTextSurname());
                    setTextEmpty(panelPurchaseObj.getTextCard());
                    setTextEmpty(panelPurchaseObj.getTextOwner());
                    setTextEmpty(panelPurchaseObj.getTextDate());
                    setTextEmpty(panelPurchaseObj.getTextCvc());
                    panelPurchaseObj.deleteLabel();
                }
        });
        ////////////////AMMINISTRATORE/////////////////
        //PANNELLO LOGIN AMMINISTRATORE
        panelBasicObj.getBtnAdminPanel().addActionListener(e-> {
                if (ci.getCurrentAdmin().isPresent()) {
                    switchPanel(panelBasicObj.getPanelWelcome(), panelAdminOneObj.getPanel());
                    panelAdminOneObj.setLabel(ci.getTotalGain());
                } else {
                    switchPanel(panelBasicObj.getPanelWelcome(), panelLoginAdminObj.getPanel());
                    panelAdminOneObj.setLabel(ci.getTotalGain());
                }
        });
        //EFFETTUA LOGIN E VA AL PANNELLO AMMINISTRATORE
        panelLoginAdminObj.getBtnLogin().addActionListener(e-> {
                boolean login = true;
                try {
                    ci.loginAdmin(panelLoginAdminObj.getUsername().getText(), panelLoginAdminObj.getPassword().getText());
                } catch (final IllegalArgumentException exc) {
                    JOptionPane.showMessageDialog(null, "Username e password devono avere da 5 a 8 caratteri");
                    login = false;
                } catch (final UnsupportedOperationException exc) {
                    JOptionPane.showMessageDialog(null, "Le credenziali non corrispondono a nessun amministratore");
                    login = false;
                }
                if (login) {
                    JOptionPane.showMessageDialog(null, "Login effettuato correttamente");
                    panelAdminOneObj.setLabel(ci.getTotalGain());
                    switchPanel(panelLoginAdminObj.getPanel(), panelAdminOneObj.getPanel());
                    panelAdminOneObj.addLabelAmm(ci.getCurrentAdmin().get());
                }
                panelLoginAdminObj.getUsername().setText("");
                panelLoginAdminObj.getPassword().setText("");
        });
        //EFFETTUA LOGOUT
        panelAdminOneObj.getBtnLogout().addActionListener(e-> {
                ci.logoutAdmin();
                switchPanel(panelAdminOneObj.getPanel(), panelBasicObj.getPanelWelcome());
        });
        //TORNA AL MENU
        panelLoginAdminObj.getBtnPrev().addActionListener(e-> {
                switchPanel(panelLoginAdminObj.getPanel(), panelBasicObj.getPanelWelcome());
        });
        //POST LOGIN TORNA AL MENU
        panelAdminOneObj.getBtnPrev().addActionListener(e-> {
                switchPanel(panelAdminOneObj.getPanel(), panelBasicObj.getPanelWelcome());
        });
        //VAI ALLA TABELLA DELLE OPERAZIONI
        panelAdminOneObj.getBtnAllOperation().addActionListener(e-> {
                for (final Integer elem : ci.getAllOperations()) {
                    panelTableObj.addTableRow(new Pair<>(elem, ci.getOperations().get(elem).getX()),
                            ci.getOperations().get(elem).getY().getDetail(),
                            ci.getOperations().get(elem).getY().getInfo(), ci.getOperationGain(elem));
                }
                switchPanel(panelAdminOneObj.getPanel(), panelTableObj.getPanel());
        });
        panelTableObj.getBtnPrev().addActionListener(e-> {
                switchPanel(panelTableObj.getPanel(), panelAdminOneObj.getPanel());
                panelTableObj.deleteTable();
        });
        //TABELLA DI OPERAZIONI PER UTENTE
        panelAdminOneObj.getBtnOperationUser().addActionListener(e-> {
                String input;
                input = JOptionPane.showInputDialog("Digita l'username");
                if (input != null) {
                    if (ci.getUserOperations(input).isEmpty()) {
                        JOptionPane.showMessageDialog(null, "Utente non trovato");
                    } else {
                        JOptionPane.showMessageDialog(null, "Utente trovato");
                        for (final Integer elem : ci.getUserOperations(input)) {
                            panelTableObj.addTableRow(new Pair<>(elem, ci.getOperations().get(elem).getX()),
                                    ci.getOperations().get(elem).getY().getDetail(),
                                    ci.getOperations().get(elem).getY().getInfo(), ci.getOperationGain(elem));
                         }
                        switchPanel(panelAdminOneObj.getPanel(), panelTableObj.getPanel());
                    }
                }
        });
        //TABELLA OPERAZIONI PER TIPO
        panelAdminOneObj.getBtnOperationType().addActionListener(e-> {
                final List<Object> values = new ArrayList<>();
                for (final String type : ci.getOperationTypes(ci.getAllOperations())) {
                    values.add(type);
                }
                final Object input = JOptionPane.showInputDialog(null, "Scegli l'operazione da visualizzare", "Operazioni", JOptionPane.INFORMATION_MESSAGE, null, values.toArray(), values.get(0));
                if (input != null) {
                    for (final Integer elem : ci.getTypeOperations(input.toString())) {
                        panelTableObj.addTableRow(new Pair<>(elem, ci.getOperations().get(elem).getX()),
                                ci.getOperations().get(elem).getY().getDetail(),
                                ci.getOperations().get(elem).getY().getInfo(), ci.getOperationGain(elem));
                    }
                    switchPanel(panelAdminOneObj.getPanel(), panelTableObj.getPanel());
                }
        });
        //VISUALIZZA OPERAZIONI PER TIPO E UTENTE
        panelAdminOneObj.getBtnOperationUserType().addActionListener(e-> {
                String input;
                input = JOptionPane.showInputDialog("Digita l'username");
                if (input != null) {
                    if (ci.getUserOperations(input).isEmpty()) {
                        JOptionPane.showMessageDialog(null, "Utente non trovato");
                    } else {
                        final List<Object> values = new ArrayList<>();
                        for (final String type : ci.getOperationTypes(ci.getUserOperations(input))) {
                            values.add(type);
                        }
                        final Object input2 = JOptionPane.showInputDialog(null, "Scegli l'operazione da visualizzare", "Operazioni", JOptionPane.INFORMATION_MESSAGE, null, values.toArray(), values.get(0));
                        if (input2 != null) {
                            for (final Integer elem : ci.getUserAndTypeOperations(input, input2.toString())) {
                                panelTableObj.addTableRow(new Pair<>(elem, ci.getOperations().get(elem).getX()),
                                        ci.getOperations().get(elem).getY().getDetail(),
                                        ci.getOperations().get(elem).getY().getInfo(), ci.getOperationGain(elem));
                            }
                            switchPanel(panelAdminOneObj.getPanel(), panelTableObj.getPanel());
                        }
                    }
                }
        });
        this.setLocationRelativeTo(null);
        this.setTitle("SKICENTER");
        this.setResizable(false);
        this.setSize(FrameSize.WIDTH.getValue(), FrameSize.HEIGHT.getValue());
        this.setLocationByPlatform(true);
        this.getContentPane().add(panelBasicObj.getPanelBase());
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setVisible(true);
    }
    /**
     * main class.
     * @param args
     *          args
     * @throws IOException
     *          exc
     */
    public static void main(final String[] args) throws IOException {
        new SkiCenterGUI();
    }
}