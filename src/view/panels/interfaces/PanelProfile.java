package view.panels.interfaces;

import javax.swing.JButton;

import model.admin.Pair;
/**
 * 
 * panel profile interface.
 *
 */
public interface PanelProfile extends Panel {
    /**
     * 
     * @param oper operation
     * @param descr description of the operation
     * @param detail details of the operation
     * @param gain price of the operation
     */
    void addTableRow(Pair<Integer, String> oper, String descr, String detail, Pair<String, String> gain);
    /**
     * delete the table.
     */
    void deleteTable();
    /**
     * @return the button to add an ad
     */
    JButton getBtnAddAd();
    /**
     * @param n name user
     */
    void addLabel(String n);
    /**
     * delete label name.
     */
    void deleteLabel();
}
