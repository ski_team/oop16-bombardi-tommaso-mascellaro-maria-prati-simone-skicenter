package view.panels.interfaces;

import javax.swing.JButton;
/**
 * 
 * panel user one interface.
 *
 */
public interface PanelUserOne  extends Panel {
    /**
     * 
     * @return button for panel skipass
     */
    JButton getBtnSkipass();
    /**
     * 
     * @return button for panel buy
     */
    JButton getBtnBuy();
    /**
     * 
     * @return button for panel rent
     */
    JButton getBtnRent();
    /**
     * 
     * @return button for panel storage
     */
    JButton getBtnStorage();
    /**
     * 
     * @return button for panel instructor
     */
    JButton getBtnInstructor();
    /**
     * 
     * @return button for panel cart
     */
    JButton getBtnCart();
    /**
     * PRATI.
     * @return button for panel with used ads
     */
    JButton getBtnUsedAds();
}
