package view.panels.interfaces;

import javax.swing.JButton;
import javax.swing.JTextField;
/**
 * Panel profile interface.
 */
public interface PanelLoginProfile extends Panel {
    /**
     * @return the button login
     */
    JButton getBtnLogin();
    /**
     * @return the button register
     */
    JButton getBtnReg();
    /**
     * @return the textField user login
     */
    JTextField getTextUser();
    /**
     * @return the textField password login
     */
    JTextField getTextPass();
    /**
     * @return the textField new user userName
     */
    JTextField getTextUser2();
    /**
     * @return the textField new user password
     */
    JTextField getTextPass2();
    /**
     * @return the textField new user name
     */
    JTextField getTextName();
    /**
     * @return the textField new user surname
     */
    JTextField getTextSurname();
}