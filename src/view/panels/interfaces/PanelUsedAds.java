package view.panels.interfaces;

import javax.swing.JButton;
import javax.swing.JTextField;

import model.admin.Pair;
/**
 * panel Used Items Ads interface.
 */
public interface PanelUsedAds extends Panel {
    /**
     * @param num of the ad
     * @param details of the ad
     */
    void addTableRow(Integer num,  Pair<String, Pair<String, Double>> details);
    /**
     * @param i number of the add to delete
     */
    void deleteAd(Integer i);
    /**
     * delete table.
     */
    void deleteTable();
    /**
     * @return the button to go to cart
     */
    JButton getBtnCart();
    /**
     * @return the button to add to the cart
     */
    JButton getBtnAddCart();
    /**
     * @return the index of the ad to buy written in textField by user
     */
    int getObjectBuy();
    /**
     * @return the textField
     */
    JTextField getAd();
}
