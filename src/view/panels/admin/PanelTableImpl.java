package view.panels.admin;


import java.awt.BorderLayout;

import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

import model.admin.Pair;
import view.panels.FrameSize;
import view.panels.interfaces.PanelTable;
/**
 * 
 * panel table class.
 *
 */
public class PanelTableImpl extends JPanel implements PanelTable {

    private static final long serialVersionUID = 1L;
    private final JButton btnRetMenu;
    private final DefaultTableModel model;
    private int count;
    private static final int COL1 = FrameSize.WIDTH.getValue() / 12;
    private static final int COL2 = FrameSize.WIDTH.getValue() / 10;
    private static final int COL3 = FrameSize.WIDTH.getValue() / 5;
    private static final int COL4 = (int) (FrameSize.WIDTH.getValue() / 3.1);
    private static final int COL5 = FrameSize.WIDTH.getValue() / 7;


    /**
     * panel table constructor.
     */
    public PanelTableImpl() {
        super();
        this.count = 0;
        final BorderLayout bl = new BorderLayout();
        this.setLayout(bl);
        this.btnRetMenu = new JButton("Pagina precedente");
        this.add(btnRetMenu, BorderLayout.SOUTH);
        this.model = new DefaultTableModel();
        model.addColumn("Num op.");
        model.addColumn("Username"); 
        model.addColumn("Tipo op."); 
        model.addColumn("Dettaglio op."); 
        model.addColumn("Incasso op."); 
        model.addColumn("Guadagno op.");
        final JTable table = new JTable(model);
        table.setEnabled(false);
        table.getColumn("Num op.").setPreferredWidth(COL1);
        table.getColumn("Username").setPreferredWidth(COL2);
        table.getColumn("Tipo op.").setPreferredWidth(COL3);
        table.getColumn("Dettaglio op.").setPreferredWidth(COL4);
        table.getColumn("Incasso op.").setPreferredWidth(COL5);
        table.getColumn("Guadagno op.").setPreferredWidth(COL5);
        final JScrollPane sp = new JScrollPane(table);
        this.add(sp, BorderLayout.CENTER);
        this.setVisible(false);
    }
    @Override
    public void addTableRow(final Pair<Integer, String> oper, final String descr, final String detail, final Pair<String, String> gain) {
        model.addRow(new Object[]{oper.getX(), oper.getY(), descr, detail, gain.getX() + " Euro", gain.getY() + " Euro"});
        this.count++;
    }
    @Override
    public void deleteTable() {
        for (int i = this.count - 1; i >= 0; i--) {
            model.removeRow(i);
        }
        this.count = 0;
    }
    @Override
    public JPanel getPanel() {
        return this;
    }
    @Override
    public JButton getBtnPrev() {
        return this.btnRetMenu;
    }
}
