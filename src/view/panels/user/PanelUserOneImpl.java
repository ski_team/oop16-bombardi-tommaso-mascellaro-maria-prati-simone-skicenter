package view.panels.user;

import java.awt.Image;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;

import view.panels.FrameSize;
import view.panels.interfaces.PanelUserOne;

/**
 * 
 * panel user class.
 *
 */
public class PanelUserOneImpl extends JPanel implements PanelUserOne {

    private static final long serialVersionUID = 1L;
    private final JButton btnRent;
    private final JButton btnBuy;
    private final JButton btnSkipass;
    private final JButton btnInstructor;
    private final JButton btnStorage;
    private final JButton btnTornaMenu;
    //PRATI
    private final JButton btnUsedAds;

    private final JButton btnCart;

    private static final int W1 = FrameSize.WIDTH.getValue() / 20;
    private static final int W2 = FrameSize.WIDTH.getValue() / 2;
    private static final int W3 = FrameSize.WIDTH.getValue() / 4;
    private static final int W4 = (int) (FrameSize.WIDTH.getValue() / 1.8);
    private static final int W5 = (int) (FrameSize.WIDTH.getValue() / 1.2);
    private static final int W6 = FrameSize.WIDTH.getValue() / 12;
    private static final int H1 = FrameSize.HEIGHT.getValue() / 20;
    private static final int H2 = FrameSize.HEIGHT.getValue() / 10;
    private static final int H3 = (int) (FrameSize.HEIGHT.getValue() / 1.3);
    private static final int H4 = FrameSize.HEIGHT.getValue() / 5;
    private static final int H5 = (int) (FrameSize.HEIGHT.getValue() / 2.8);


/**
 * panel user constructor.
 */
    public PanelUserOneImpl() {
        super();
        final ImageIcon img = new ImageIcon(getClass().getResource("/sfondo.jpg"));
        final Image scaledImage = img.getImage().getScaledInstance(FrameSize.WIDTH.getValue(), FrameSize.HEIGHT.getValue(), Image.SCALE_DEFAULT);
        img.setImage(scaledImage);
        final JLabel labelImg = new JLabel(img);
        labelImg.setBounds(0, 0, FrameSize.WIDTH.getValue(), FrameSize.HEIGHT.getValue());
        this.btnBuy = new JButton("ACQUISTO");
        this.btnBuy.setBounds(W1, H1, W3, H2);
        labelImg.add(btnBuy);
        this.btnRent = new JButton("NOLEGGIO");
        this.btnRent.setBounds(W2, H1, W3, H2);
        labelImg.add(btnRent);
        this.btnSkipass = new JButton("SKIPASS");
        this.btnSkipass.setBounds(W1, H4, W3, H2);
        labelImg.add(btnSkipass);
        this.btnStorage = new JButton("DEPOSITO");
        this.btnStorage.setBounds(W2, H4, W3, H2);
        labelImg.add(btnStorage);
        this.btnInstructor = new JButton("MAESTRO");
        this.btnInstructor.setBounds(W1, H5, W3, H2);
        labelImg.add(btnInstructor);
        //PRATI
        this.btnUsedAds = new JButton("ANNUNCI USATO");
        this.btnUsedAds.setBounds(W2, H5, W3, H2);
        labelImg.add(btnUsedAds);
        //FINE
        this.btnTornaMenu = new JButton("Torna al menu'");
        this.btnTornaMenu.setBounds(W4, H3, W3, H2);
        labelImg.add(btnTornaMenu);
        final ImageIcon imgCart = new ImageIcon(getClass().getResource("/carrello.gif"));
        this.btnCart = new JButton(imgCart);
        final Image scaledImages = imgCart.getImage().getScaledInstance((int) (FrameSize.WIDTH.getValue() / 12), (int) (FrameSize.HEIGHT.getValue() / 10), Image.SCALE_DEFAULT);
        imgCart.setImage(scaledImages);
        this.btnCart.setBounds(W5, H3, W6, H2);
        labelImg.add(btnCart);
        this.add(labelImg);
        this.setLayout(null);
        this.setVisible(false);
    }

    @Override
    public JPanel getPanel() {
        return this;
    }
    @Override
    public JButton getBtnPrev() {
        return this.btnTornaMenu;
    }
    @Override
    public JButton getBtnSkipass() {
        return this.btnSkipass;
    }
    @Override
    public JButton getBtnBuy() {
        return this.btnBuy;
    }
    @Override
    public JButton getBtnRent() {
        return this.btnRent;
    }
    @Override
    public JButton getBtnStorage() {
        return this.btnStorage;
    }
    @Override
    public JButton getBtnInstructor() {
        return this.btnInstructor;
    }
    @Override
    public JButton getBtnCart() {
        return this.btnCart;
    }
    @Override
    public JButton getBtnUsedAds() {
        return this.btnUsedAds;
    }
}
