package view.panels.user;

import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;

import model.admin.Pair;
import view.panels.FrameSize;
import view.panels.interfaces.PanelUsedAds;

/**
 * 
 * panel cart class.
 *
 */
public class PanelUsedAdsImpl extends JPanel implements PanelUsedAds {

    private static final long serialVersionUID = 1L;
    private final JButton btnRetMenu;
    private final JButton btnAddCart;
    private final JButton btnCart;
    private final DefaultTableModel model;
    private final JTextField text;
    private int count;
    //COLORE
    private static final int C1 = 171;
    private static final int C2 = 205;
    private static final int C3 = 239;

    private static final int COL1 = FrameSize.WIDTH.getValue() / 12;
    private static final int COL2 = FrameSize.WIDTH.getValue() / 8;
    private static final int COL3 = (int) (FrameSize.WIDTH.getValue() / 3);
    private static final int COL4 = FrameSize.WIDTH.getValue() / 5;
    private static final int LIM = (int) (FrameSize.HEIGHT.getValue() / 1.5);
    private static final int WC = FrameSize.WIDTH.getValue() / 4;
    private static final int WC1 = FrameSize.WIDTH.getValue() / 15;
    private static final int WC2 = FrameSize.WIDTH.getValue() / 20;
    private static final int WC3 = (int) (FrameSize.WIDTH.getValue() / 3.65);
    private static final int WC4 = (int) (FrameSize.WIDTH.getValue() / 2.8);
    private static final int WC5 = (int) (FrameSize.WIDTH.getValue() / 1.5);

    private static final int HC = FrameSize.HEIGHT.getValue() / 15;
    private static final int HC1 = FrameSize.HEIGHT.getValue() / 60;
    private static final int HC2 = FrameSize.HEIGHT.getValue() / 8;

    private static final int FONT1 = FrameSize.WIDTH.getValue() / 50;



    /**
     * panel cart constructor.
     */
    public PanelUsedAdsImpl() {
        super();
        this.count = 0;
        final JPanel panelSouth = new JPanel();
        final GridBagLayout layout = new GridBagLayout();
        final GridBagConstraints lim = new GridBagConstraints();
        this.setLayout(layout);
        //TABELLA CARRELLO
        this.model = new DefaultTableModel();
        model.addColumn("Num ads."); 
        model.addColumn("Utente venditore."); 
        model.addColumn("Dettaglio ogg.");
        model.addColumn("Costo ogg.");
        final JTable table = new JTable(model);
        table.setEnabled(false);
        table.getColumn("Num ads.").setPreferredWidth(COL1);
        table.getColumn("Utente venditore.").setPreferredWidth(COL2);
        table.getColumn("Dettaglio ogg.").setPreferredWidth(COL3);
        table.getColumn("Costo ogg.").setPreferredWidth(COL4);
        final JScrollPane sp = new JScrollPane(table);
        final Component c0 = sp;
        lim.gridx = 0;
        lim.gridy = 0;
        lim.weightx = 1;
        lim.weighty = 1;
        lim.ipadx = FrameSize.WIDTH.getValue();
        lim.ipady = LIM;
        lim.fill = GridBagConstraints.NONE;
        lim.anchor = GridBagConstraints.NORTH;
        layout.setConstraints(c0, lim);
        this.add(c0);
        //SFONDO
        final Color bluette = new Color(C1, C2, C3);
        this.setBackground(bluette);
        //OPERAZIONE DA ELIMINARE
        final JLabel lab = new JLabel("Oggetto da acquistare");
        lab.setBounds(WC2, HC1, WC, HC);
        lab.setFont(new Font("Tahoma", Font.PLAIN,  FONT1));
        panelSouth.add(lab);
        this.text = new JTextField();
        this.text.setBounds(WC3, HC1, WC1, HC);
        panelSouth.add(text);
        this.btnAddCart = new JButton("Aggiungi al carrello");
        this.btnAddCart.setBounds(WC4, HC1, WC, HC);
        panelSouth.add(btnAddCart);
        //BOTTONE PAGINA PRECEDENTE
        this.btnRetMenu = new JButton("Pagina precedente");
        this.btnRetMenu.setBounds(WC5, HC2, WC, HC);
        panelSouth.add(btnRetMenu);
        //BOTTONE CARRELLO
        this.btnCart = new JButton("Vai al carrello");
        this.btnCart.setBounds(WC4, HC2, WC, HC);
        panelSouth.add(btnCart);
        final Component c1 = panelSouth;
        lim.gridx = 0;
        lim.gridy = 1;
        lim.weightx = 1;
        lim.weighty = LIM;
        lim.ipadx = 0;
        lim.ipady = LIM;
        lim.fill = GridBagConstraints.HORIZONTAL;
        lim.anchor = GridBagConstraints.CENTER;
        layout.setConstraints(c1, lim);
        this.add(c1);
        panelSouth.setLayout(null);
        this.setVisible(false);
    }
    @Override
    public void addTableRow(final Integer num,  final Pair<String, Pair<String, Double>> details) {
        model.addRow(new Object[]{num, details.getX(), details.getY().getX(), details.getY().getY().toString() + " Euro"});
        this.count++;
    }
    @Override
    public void deleteAd(final Integer i) {
        model.removeRow(i - 1);
    }
    @Override
    public void deleteTable() {
        for (int i = this.count - 1; i >= 0; i--) {
            model.removeRow(i);
        }
        this.count = 0;
    }
    @Override
    public JPanel getPanel() {
        return this;
    }
    @Override
    public JButton getBtnPrev() {
        return this.btnRetMenu;
    }
    @Override
    public JButton getBtnCart() {
        return this.btnCart;
    }
    @Override
    public JButton getBtnAddCart() {
        return this.btnAddCart;
    }
    @Override
    public int getObjectBuy() {
        return Integer.parseInt(this.text.getText());
    }
    @Override
    public JTextField getAd() {
        return this.text;
    }
}
