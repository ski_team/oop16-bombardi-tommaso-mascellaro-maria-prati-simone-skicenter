package view.panels.user;

import java.awt.Color;
import java.awt.Container;
import java.awt.Font;
import java.awt.GridLayout;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import view.panels.FrameSize;
import view.panels.interfaces.PanelLoginProfile;
/**
 * 
 * Class of panelPurchase.
 *
 */
public class PanelLoginProfileImpl extends JPanel implements PanelLoginProfile {

    private static final long serialVersionUID = 1L;
    private final JTextField username;
    private final JTextField password;
    private final JTextField name;
    private final JTextField surname;
    private final JTextField username2;
    private final JTextField password2;
    private final JButton btnLogin;
    private final JButton btnReg;
    private final JButton btnRetMenu;
    private static final int FONT_LAB = FrameSize.WIDTH.getValue() / 40;

  //COLORE
    private static final int C1 = 171;
    private static final int C2 = 205;
    private static final int C3 = 239;

    /**
     * Constructor of panelPurchase.
     */
    public PanelLoginProfileImpl() {
        super();
        final Container cont = new Container();
        cont.setLayout(new GridLayout(2, 1));

        //PANNELLI PRINCIPALI
        final JPanel pan1 = new JPanel();
        final JPanel pan2 = new JPanel();
        final JPanel pan3 = new JPanel();

        //CONTAINER PER PARTE SUPERIORE
        final Container x = new Container();
        x.setLayout(new GridLayout(1, 2));
        x.add(pan1);
        x.add(pan2);

        //CONTAINER PARTE INFERIORE
        final Container y = new Container();
        y.setLayout(new GridLayout(1, 1));
        y.add(pan3);

        pan1.setLayout(new GridLayout(8, 2));
        pan2.setLayout(new GridLayout(8, 2));
        pan3.setLayout(new GridLayout(8, 2));
        final TitledBorder titleBorder = new TitledBorder(new LineBorder(Color.BLACK), "Sei già registrato? Accedi");
        titleBorder.setTitleJustification(TitledBorder.CENTER);
        titleBorder.setTitleColor(Color.RED);
        titleBorder.setTitleFont(new Font(Font.SANS_SERIF, Font.BOLD, FONT_LAB));
        pan1.setBorder(titleBorder);
        final TitledBorder titleBorder2 = new TitledBorder(new LineBorder(Color.BLACK), "Nuovo utente? Registrati ora");
        titleBorder2.setTitleColor(Color.RED);
        titleBorder2.setTitleJustification(TitledBorder.CENTER);
        titleBorder2.setTitleFont(new Font(Font.SANS_SERIF, Font.BOLD, FONT_LAB));
        pan2.setBorder(titleBorder2);

        //USERNAME
        final JLabel labUser = new JLabel("Username");
        pan1.add(labUser);
        this.username = new JTextField();
        pan1.add(username);

        final JLabel labUser2 = new JLabel("Username");
        pan2.add(labUser2);
        this.username2 = new JTextField();
        pan2.add(username2);
        //PASSWORD
        final JLabel labPass = new JLabel("Password");
        pan1.add(labPass);
        this.password = new JPasswordField();
        pan1.add(password);
        final JLabel labPass2 = new JLabel("Password");
        pan2.add(labPass2);
        this.password2 = new JPasswordField();
        pan2.add(password2);

        //NOME
        final JLabel labName = new JLabel("Nome");
        pan2.add(labName);
        this.name = new JTextField();
        pan2.add(name);

        //COGNOME
        final JLabel labSurname = new JLabel("Cognome");
        pan2.add(labSurname);
        this.surname = new JTextField();
        pan2.add(surname);

        //BOOTTONE LOGIN
        this.btnLogin = new JButton("Login");
        pan1.add(btnLogin);

        final JLabel lab = new JLabel("Attenzione: per accedere devi inserire");
        final JLabel lab1 = new JLabel("username e password da 5 a 8 caratteri");
        lab.setForeground(Color.blue);
        lab1.setForeground(Color.blue);
        pan1.add(lab);
        pan1.add(lab1);

        //BOTTONE REGISTRATI
        this.btnReg = new JButton("Registrati e accedi");
        pan2.add(btnReg);

        //BOTTONE TORNA AL MENU
        this.btnRetMenu = new JButton("Pagina precedente");
        pan3.add(btnRetMenu);
        //SFONDO
        final Color bluette = new Color(C1, C2, C3);
        pan1.setBackground(bluette);
        pan2.setBackground(bluette);
        pan3.setBackground(bluette);
        this.setBackground(bluette);
        cont.add(x);
        cont.add(y);
        this.setLayout(new GridLayout(1, 1));
        this.add(cont);
        this.setVisible(false);
    }

    @Override
    public JPanel getPanel() {
        return this;
    }
    @Override
    public JButton getBtnPrev() {
        return this.btnRetMenu;
    }
    @Override
    public JButton getBtnLogin() {
        return this.btnLogin;
    }
    @Override
    public JButton getBtnReg() {
        return this.btnReg;
    }
    @Override
    public JTextField getTextUser() {
        return this.username;
    }
    @Override
    public JTextField getTextPass() {
        return this.password;
    }
    @Override
    public JTextField getTextUser2() {
        return this.username2;
    }
    @Override
    public JTextField getTextPass2() {
        return this.password2;
    }
    @Override
    public JTextField getTextName() {
        return this.name;
    }
    @Override
    public JTextField getTextSurname() {
        return this.surname;
    }
}
