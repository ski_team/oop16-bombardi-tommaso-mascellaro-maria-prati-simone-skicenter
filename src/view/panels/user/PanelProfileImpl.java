package view.panels.user;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

import model.admin.Pair;
import view.panels.FrameSize;
import view.panels.interfaces.PanelProfile;

/**
 * 
 * panel cart class.
 *
 */
public class PanelProfileImpl extends JPanel implements PanelProfile {

    private static final long serialVersionUID = 1L;
    private final JButton btnLogout;
    private final JButton btnAddAd;
    private final DefaultTableModel model;
    private final JLabel welcome;
    private int count = 1;

    //COLORE
    private static final int C1 = 171;
    private static final int C2 = 205;
    private static final int C3 = 239;

    private static final int COL1 = FrameSize.WIDTH.getValue() / 12;
    private static final int COL2 = FrameSize.WIDTH.getValue() / 5;
    private static final int COL3 = (int) (FrameSize.WIDTH.getValue() / 3);
    private static final int COL4 = FrameSize.WIDTH.getValue() / 4;
    private static final int WC = FrameSize.WIDTH.getValue() / 4;
    private static final int WC4 = (int) (FrameSize.WIDTH.getValue() / 2.8);
    private static final int WC5 = (int) (FrameSize.WIDTH.getValue() / 1.5);

    private static final int HC = FrameSize.HEIGHT.getValue() / 15;
    private static final int HC2 = FrameSize.HEIGHT.getValue() / 8;

    /**
     * panel cart constructor.
     */
    public PanelProfileImpl() {
        super();
        this.count = 0;
        final JPanel panelSouth = new JPanel();
        final JPanel panelNorth = new JPanel();
        final BorderLayout layout = new BorderLayout();
        this.setLayout(layout);

        //LABEL BENVENUTO
        this.welcome = new JLabel("");
        panelNorth.add(welcome);
        final Component c3 = panelNorth;
        this.add(c3, BorderLayout.NORTH);

        //TABELLA OPERAZIONI SVOLTE
        this.model = new DefaultTableModel();
        model.addColumn("Num op."); 
        model.addColumn("Tipo op."); 
        model.addColumn("Dettaglio op.");
        model.addColumn("Costo op.");
        final JTable table = new JTable(model);
        table.setEnabled(false);
        table.getColumn("Num op.").setPreferredWidth(COL1);
        table.getColumn("Tipo op.").setPreferredWidth(COL2);
        table.getColumn("Dettaglio op.").setPreferredWidth(COL3);
        table.getColumn("Costo op.").setPreferredWidth(COL4);
        final JScrollPane sp = new JScrollPane(table);
        final Component c0 = sp;
        this.add(c0, BorderLayout.CENTER);
        //SFONDO
        final Color bluette = new Color(C1, C2, C3);
        this.setBackground(bluette);
        //BOTTONE AGGIUNGI ANNUNCIO
        this.btnAddAd = new JButton("Aggiungi un annuncio");
        this.btnAddAd.setBounds(WC4, HC2, WC, HC);
        panelSouth.add(btnAddAd);
        //BOTTONE PAGINA PRECEDENTE
        this.btnLogout = new JButton("Logout");
        this.btnLogout.setBounds(WC5, HC2, WC, HC);
        panelSouth.add(btnLogout);
        final Component c1 = panelSouth;
        this.add(c1, BorderLayout.SOUTH);
        this.setVisible(false);

    }

    @Override
    public void addTableRow(final Pair<Integer, String> oper, final String descr, final String detail, final Pair<String, String> gain) {
        this.count++;
        model.addRow(new Object[]{this.count, descr, detail, gain.getX() + " Euro"});
    }
    @Override
    public void deleteTable() {
        for (int i = this.count - 1; i >= 0; i--) {
            model.removeRow(i);
        }
        this.count = 0;
    }
    @Override
    public JPanel getPanel() {
        return this;
    }
    @Override
    public JButton getBtnPrev() {
        return this.btnLogout;
    }
    @Override
    public JButton getBtnAddAd() {
        return this.btnAddAd;
    }
    @Override
    public void addLabel(final String n) {
        this.welcome.setText("Benvenuto " + n + ", questi sono i tuoi acquisti:");
        this.welcome.setForeground(Color.blue);
    }
    @Override
    public void deleteLabel() {
        this.welcome.setText("");
    }
}
